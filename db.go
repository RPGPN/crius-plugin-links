package Links

// We'll use realm instead of guild / channel here because we support both GuildCrius *and* LiveCrius
const tableCreateScript = `CREATE TABLE IF NOT EXISTS links__links (
			link_id   serial primary key,
			realm_id text not null,
			platform text not null,
			link_name text not null,
			link_to   text not null
		);`

type Link struct {
	LinkID   int    `db:"link_id"`
	RealmID  string `db:"realm_id"`
	LinkName string `db:"link_name"`
	LinkTo   string `db:"link_to"`
	Platform string `db:"platform"`
}
