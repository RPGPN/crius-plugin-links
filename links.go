package Links

import (
	"context"
	"database/sql"
	"errors"
	"github.com/bwmarrin/discordgo"
	"github.com/gempir/go-twitch-irc/v2"
	"github.com/jmoiron/sqlx"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	"gitlab.com/RPGPN/crius-utils/GuildCrius"
	"gitlab.com/RPGPN/crius-utils/LiveCrius"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	goleshchat "gitlab.com/ponkey364/golesh-chat"
	"regexp"
)

var linkRegex = regexp.MustCompile("[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)")

var PluginInfo = &cc.PluginJSON{
	Name:               "Links",
	License:            "BSD-3-Clause",
	Creator:            "Ben <Ponkey364>",
	URL:                "https://gitlab.com/RPGPN/crius-plugin-links/",
	Description:        "Link storage plugin",
	SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
	Commands: []*cc.PJCommand{
		{
			HandlerName:        "newlink",
			Name:               "New Link",
			Help:               "Add a link to the storage. Usage: newlink [name] [URL]",
			Activator:          "newlink",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
		},
		{
			HandlerName:        "link",
			Name:               "Get Link",
			Help:               "Get a link. Usage: link [name]",
			Activator:          "link",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
		},
		{
			HandlerName:        "links",
			Name:               "List Links",
			Help:               "Get a list of all the links for the server.",
			Activator:          "links",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
		},
		{
			HandlerName:        "removelink",
			Name:               "Remove Link",
			Help:               "Remove a link. Usage: removelink [name]",
			Activator:          "removelink",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
		},
	},
}

func testForLink(data string) bool {
	return linkRegex.Match([]byte(data))
}

type links struct {
	db *sqlx.DB
}

func Setup(ctx context.Context) (map[string]cc.CommandHandler, error) {
	// get stuff we need from ctx
	s := CriusUtils.GetSettings(ctx)

	db := s.GetDB()

	// run the table creation script from db.go
	// create a transaction
	tx := db.MustBegin()

	// run the script
	_, err := tx.Exec(tableCreateScript)
	if err != nil {
		return nil, err
	}

	// done
	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	l := &links{
		db: db,
	}

	return map[string]cc.CommandHandler{
		"newlink":    l.NewLink,
		"link":       l.Link,
		"links":      l.Links,
		"removelink": l.RemLink,
	}, nil
}

func (l *links) NewLink(m *cc.MessageContext, args []string) error {
	if len(args) < 2 {
		m.Send("you must specify a link name and url. Usage: `newlink [name] [url]`")
		return nil
	}

	// is what we've been passed in args[1] a link?
	// (yes, I know i could not bother, but this is the *links* module not the *info* plugin.)
	if !testForLink(args[1]) {
		m.Send("I don't think that's a link.")
		return nil
	}

	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID
	switch platform {
	case cc.PlatformDiscord:
		{
			message := m.PlatformData.(*discordgo.MessageCreate)
			isAdmin, err := m.Context.Value("permissions").(GuildCrius.Permissions).IsServerAdmin(message.Author.ID, message.GuildID)
			if err != nil {
				return err
			}

			if !isAdmin {
				m.Send("Only server admins can add new links.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)

			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("Only the streamer can add new links.")
				return nil
			}
		}
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitch.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("Only the streamer or moderators can add new links.")
				return nil
			}
		}
	}

	_, err := l.db.Exec("INSERT INTO links__links (realm_id, platform, link_name, link_to) VALUES ($1, $2, $3, $4)",
		realmID, platform.ToString(), args[0], args[1])
	if err != nil {
		return err
	}

	m.Send("Added link " + args[0])

	return nil
}

func (l *links) Link(m *cc.MessageContext, args []string) error {
	if len(args) == 0 {
		m.Send("you must specify a link name, do `links` for a list.")
		return nil
	}

	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID

	link := &Link{}
	err := l.db.Get(link, "SELECT * FROM links__links WHERE platform=$1 AND realm_id=$2 AND link_name=$3",
		platform.ToString(), realmID, args[0])
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if errors.Is(err, sql.ErrNoRows) {
		m.Send("there is no link with the name " + args[0])
		return nil
	}

	m.Send(link.LinkName + " : " + link.LinkTo)

	return nil
}

func (l *links) Links(m *cc.MessageContext, _ []string) error {
	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID

	// pull any links from the db
	links := []*Link{}
	err := l.db.Select(&links, "SELECT * FROM links__links WHERE realm_id=$1 AND platform=$2", realmID, platform.ToString())
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	// apparently we won't get a ErrNoRows, but I've seen it in the past so just in case.
	if errors.Is(err, sql.ErrNoRows) || len(links) == 0 {
		m.Send("this realm has no links stored.")
		return nil
	}

	// concat and send.
	msg := "This realm has the following links: "
	for i, link := range links {
		msg += link.LinkName
		if i < len(links)-1 {
			msg += ", "
		}
	}

	m.Send(msg)

	return nil
}

func (l *links) RemLink(m *cc.MessageContext, args []string) error {
	if len(args) == 0 {
		m.Send("you must specify a link name. Usage: `removelink [name]`")
		return nil
	}

	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID
	switch platform {
	case cc.PlatformDiscord:
		{
			message := m.PlatformData.(*discordgo.MessageCreate)
			isAdmin, err := m.Context.Value("permissions").(GuildCrius.Permissions).IsServerAdmin(message.Author.ID, message.GuildID)
			if err != nil {
				return err
			}

			if !isAdmin {
				m.Send("Only server admins can remove links.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)

			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("Only the streamer can remove links.")
				return nil
			}
		}
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitch.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("Only the streamer or moderators can remove links.")
				return nil
			}
		}
	}

	_, err := l.db.Exec("DELETE FROM links__links WHERE realm_id=$1 AND platform=$2 AND link_name=$3",
		realmID, platform.ToString(), args[0])
	if err != nil {
		return err
	}

	m.Send("Removed link " + args[0])

	return nil
}
